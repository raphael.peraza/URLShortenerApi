FROM openjdk:8-jre-alpine
WORKDIR /app
# COPY [ "./target/*", "./target/" ]
COPY target .
CMD [ "java", "-jar", "URLShortenerApi-0.0.1-SNAPSHOT.jar" ]
